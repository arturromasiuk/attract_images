package romasiuk.com.attracttest.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_item_list.view.*
import romasiuk.com.attracttest.R
import romasiuk.com.attracttest.adapter.ImageListAdapter
import romasiuk.com.attracttest.view_model.ImageListViewModel


class ImageListFragment : Fragment() {

    private lateinit var viewModel: ImageListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val viewLayout = inflater.inflate(R.layout.fragment_item_list, container, false)

        viewModel = ViewModelProviders.of(requireActivity()).get(ImageListViewModel::class.java)

        val viewAdapter = ImageListAdapter(activity as ImageListAdapter.OnImageClickListener, viewModel.imageManager)

        val columns = resources.getInteger(R.integer.image_list_columns)
        viewLayout.rv_image_list.apply {
            layoutManager = GridLayoutManager(context, columns)
            adapter = viewAdapter
        }

        viewModel.imageList.observe(this, Observer { imageList ->
            viewAdapter.setList(imageList)
        })

        setupSearch(viewLayout, viewModel)

        return viewLayout
    }

    private fun setupSearch(
        viewLayout: View,
        imageListViewModel: ImageListViewModel
    ) {
        viewLayout.search_view.setIconifiedByDefault(false)
        viewLayout.search_view.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(text: String): Boolean {
                imageListViewModel.filter(text)
                return true
            }
        })
    }


}
