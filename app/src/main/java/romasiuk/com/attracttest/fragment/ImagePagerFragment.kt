package romasiuk.com.attracttest.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.fragment_image_pager.*
import kotlinx.android.synthetic.main.fragment_image_pager.view.*
import romasiuk.com.attracttest.R
import romasiuk.com.attracttest.adapter.ImagePagerAdapter
import romasiuk.com.attracttest.view_model.ImageListViewModel

private const val SELECTED_POSITION = "SELECTED_POSITION"

class ImagePagerFragment : Fragment() {

    private lateinit var viewModel: ImageListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewLayout = inflater.inflate(R.layout.fragment_image_pager, container, false)

        viewModel = ViewModelProviders.of(requireActivity()).get(ImageListViewModel::class.java)
        val viewAdapter = ImagePagerAdapter(viewModel.imageManager)
        val pager = viewLayout.pager
        pager.apply {
            adapter = viewAdapter
        }
        viewModel.imageList.observe(this, Observer { imageList ->
            viewAdapter.setList(imageList)
        })

        scrollToSelectedPage(savedInstanceState, pager)

        return viewLayout
    }

    private fun scrollToSelectedPage(
        savedInstanceState: Bundle?,
        pager: ViewPager2
    ) {
        var selectedPosition = viewModel.selectedPosition
        if (savedInstanceState != null) {
            selectedPosition = savedInstanceState.getInt(SELECTED_POSITION)
        }
        pager.post { pager.setCurrentItem(selectedPosition, false) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SELECTED_POSITION, pager.currentItem)
    }

}
