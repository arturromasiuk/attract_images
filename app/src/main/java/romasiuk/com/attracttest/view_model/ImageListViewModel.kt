package romasiuk.com.attracttest.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import romasiuk.com.attracttest.BasicApp
import romasiuk.com.attracttest.api.ImageApi
import romasiuk.com.attracttest.model.ImageItem


class ImageListViewModel(application: Application) : AndroidViewModel(application) {
    private val imageApi:ImageApi = (application as BasicApp).getApi()
    val imageManager = (application as BasicApp).getImageManager()
    private val loadedImageList: LiveData<List<ImageItem>>
    private val filteredImageList:MutableLiveData<List<ImageItem>>
    var imageList = MediatorLiveData<List<ImageItem>>()
    var selectedPosition:Int = 0

    init {
        loadedImageList = imageApi.getItemList()
        filteredImageList = MutableLiveData()
        imageList.addSource(loadedImageList){
            imageList.value = it
        }
        imageList.addSource(filteredImageList){
            imageList.value = it
        }
    }

    fun filter(query:String){
        if (query.isEmpty()){
            filteredImageList.value = loadedImageList.value
        } else {
            val lowerCaseQuery = query.toLowerCase()
            val results = mutableListOf<ImageItem>()
            loadedImageList.value?.let {originalList ->
                for (item in originalList) {
                    if (item.name.toLowerCase().contains(lowerCaseQuery)) {
                        results.add(item)
                    }
                }
                filteredImageList.value = results
            }
        }
    }

}
