package romasiuk.com.attracttest.manager

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import romasiuk.com.attracttest.model.ImageItem
import java.io.File
import java.io.FileOutputStream
import java.net.URL


class ImageManager(private val context: Context) {

    private fun download(url: String): Bitmap {
        val inputStream = URL(url).openStream()
        return BitmapFactory.decodeStream(inputStream)
    }

    private fun save(bitmap: Bitmap, id: Int) {
        var file = File(context.filesDir, "Images")
        if (!file.exists()) {
            file.mkdir()
        }
        file = File(file, "${id}.jpg")
        val out = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, out)
        out.flush()
        out.close()
    }

    private fun getFromFile(id: Int): Bitmap? {
        val path = "${File(context.filesDir, "Images").path}/${id}.jpg"
        val imgFile = File(path)
        return if (imgFile.exists()) BitmapFactory.decodeFile(imgFile.absolutePath) else null
    }

    private fun getImage(imageItem: ImageItem): Bitmap? {
        try {
            val bitmapFromFile = getFromFile(imageItem.id)
            bitmapFromFile?.let {
                return it
            } ?: run {
                val bitmapFromURL = download(imageItem.image)
                save(bitmapFromURL, imageItem.id)
                return bitmapFromURL
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    fun getImageBitmap(imageItem: ImageItem, onImageReady: (Bitmap) -> (Unit)) {
        GlobalScope.launch {
            val bitmap = getImage(imageItem)
            bitmap?.let(onImageReady)
        }
    }


}