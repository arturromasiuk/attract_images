package romasiuk.com.attracttest.activity

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*
import romasiuk.com.attracttest.R
import romasiuk.com.attracttest.adapter.ImageListAdapter
import romasiuk.com.attracttest.fragment.ImageListFragment
import romasiuk.com.attracttest.fragment.ImagePagerFragment
import romasiuk.com.attracttest.view_model.ImageListViewModel


class MainActivity : AppCompatActivity(), ImageListAdapter.OnImageClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        val actionbar = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)

        val menuDrawable = ContextCompat.getDrawable(this, R.drawable.menu)
        actionbar?.setHomeAsUpIndicator(menuDrawable)
        if (savedInstanceState == null)
            addImageListFragment()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onImageClick(position: Int) {
        hideSoftKeyboard()
        openImagePageFragment(position)
    }

    private fun addImageListFragment() {
        val fragment = ImageListFragment()
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container, fragment, ImageListFragment::javaClass.name).commit()
    }

    private fun openImagePageFragment(position: Int) {
        val newFragment = ImagePagerFragment()
        ViewModelProviders.of(this).get(ImageListViewModel::class.java).selectedPosition = position
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, newFragment)
            .addToBackStack(null)
            .commit()
    }

    private fun hideSoftKeyboard() {
        val inputMethodManager: InputMethodManager = getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            currentFocus?.windowToken, 0
        )
    }

}
