package romasiuk.com.attracttest

import android.app.Application
import romasiuk.com.attracttest.api.AttractImageApi
import romasiuk.com.attracttest.api.ImageApi
import romasiuk.com.attracttest.manager.ImageManager


class BasicApp: Application() {

    private lateinit var api:ImageApi
    private lateinit var imageManager: ImageManager

    override fun onCreate() {
        super.onCreate()
        api = AttractImageApi()
        imageManager = ImageManager(this)
    }

    fun getApi():ImageApi{
        return api
    }

    fun getImageManager():ImageManager{
        return imageManager
    }
}