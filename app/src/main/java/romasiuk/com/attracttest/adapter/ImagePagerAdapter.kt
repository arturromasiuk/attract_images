package romasiuk.com.attracttest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import romasiuk.com.attracttest.R
import romasiuk.com.attracttest.manager.ImageManager
import romasiuk.com.attracttest.model.ImageItem
import java.text.SimpleDateFormat
import java.util.*


class ImagePagerAdapter(private val imageManager: ImageManager) :
    androidx.recyclerview.widget.RecyclerView.Adapter<ImagePagerAdapter.ImagePageItemVH>() {

    private var list: List<ImageItem> = emptyList()

    inner class ImagePageItemVH(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView = itemView.findViewById(R.id.tv_name)
        var descriptionTV: TextView = itemView.findViewById(R.id.tv_description)
        var timeTV: TextView = itemView.findViewById(R.id.tv_time)
        var imageIV: ImageView = itemView.findViewById(R.id.iv_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagePageItemVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_image_page, parent, false)
        return ImagePageItemVH(v)
    }

    override fun onBindViewHolder(holder: ImagePageItemVH, position: Int) {
        val imageItem = list[position]
        holder.nameTV.text = imageItem.name
        holder.descriptionTV.text = imageItem.description
        holder.timeTV.text = imageItem.formatDate()
        imageManager.getImageBitmap(imageItem) { bitmap ->
            holder.imageIV.post { holder.imageIV.setImageBitmap(bitmap) }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(categoryList: List<ImageItem>) {
        list = categoryList
        notifyDataSetChanged()
    }

}