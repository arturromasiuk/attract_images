package romasiuk.com.attracttest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import romasiuk.com.attracttest.R
import romasiuk.com.attracttest.manager.ImageManager
import romasiuk.com.attracttest.model.ImageItem
import java.text.SimpleDateFormat
import java.util.*


class ImageListAdapter(private var listener: OnImageClickListener? = null, private val imageManager: ImageManager) :
    androidx.recyclerview.widget.RecyclerView.Adapter<ImageListAdapter.ImageItemVH>() {

    private var originalList: List<ImageItem> = emptyList()
    private var filteredList: List<ImageItem> = emptyList()

    inner class ImageItemVH(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView = itemView.findViewById(R.id.tv_name)
        var timeTV: TextView = itemView.findViewById(R.id.tv_time)
        var imageIV: ImageView = itemView.findViewById(R.id.iv_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageItemVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
        return ImageItemVH(v)
    }

    override fun onBindViewHolder(holder: ImageItemVH, position: Int) {
        val imageItem = filteredList[position]
        holder.nameTV.text = imageItem.name
        holder.timeTV.text = imageItem.formatDate()
        holder.itemView.setOnClickListener { listener?.onImageClick(position) }

        imageManager.getImageBitmap(imageItem) { bitmap ->
            holder.imageIV.post { holder.imageIV.setImageBitmap(bitmap) }
        }
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    fun setList(categoryList: List<ImageItem>) {
        originalList = categoryList
        filteredList = categoryList
        notifyDataSetChanged()
    }

    interface OnImageClickListener {
        fun onImageClick(position: Int)
    }


}