package romasiuk.com.attracttest.model

import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

data class ImageItem(
    val id: Int,
    val name: String,
    val image: String,
    val description: String,
    val time: Long
) {
    companion object {
        fun parse(jsonObject: JSONObject): ImageItem {
            return ImageItem(
                jsonObject.getInt("itemId"),
                jsonObject.getString("name"),
                jsonObject.getString("image"),
                jsonObject.getString("description"),
                jsonObject.getLong("time")
            )
        }
    }

    fun formatDate():String{
        val formatter = SimpleDateFormat("dd-MMMM-yyyy HH:mm", Locale.getDefault())
        return formatter.format(Date(time))
    }
}
