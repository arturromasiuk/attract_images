package romasiuk.com.attracttest.api

import androidx.lifecycle.LiveData
import romasiuk.com.attracttest.model.ImageItem

interface ImageApi {
    fun getItemList():LiveData<List<ImageItem>>
}