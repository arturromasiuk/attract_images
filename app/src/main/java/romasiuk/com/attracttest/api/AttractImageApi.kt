package romasiuk.com.attracttest.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONArray
import romasiuk.com.attracttest.model.ImageItem
import java.net.URL

class AttractImageApi : ImageApi {
    override fun getItemList(): LiveData<List<ImageItem>> {
        val imageListLD = MutableLiveData<List<ImageItem>>()
        GlobalScope.launch {
            val jsonStr = URL("http://test.php-cd.attractgroup.com/test.json").readText()
            imageListLD.postValue(parseJSON(jsonStr))
        }
        return imageListLD
    }

    private fun parseJSON(jsonStr: String): List<ImageItem> {
        val imageList = mutableListOf<ImageItem>()
        try {
            val jsonArray = JSONArray(jsonStr)
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val item = ImageItem.parse(jsonObject)
                imageList.add(item)
            }
        } catch (e: Exception) {
            print(e.printStackTrace())
        }
        return imageList
    }

}